/**
 * 
 */
package com.theosterkamps.hello;

/**
 * @author bryanosterkamp
 *
 */
public class MathBot {
	
	public Double divide (double numerator, double denominator)
	{
		return numerator / denominator;
	}

}
