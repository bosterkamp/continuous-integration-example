package com.theosterkamps.hello;

public class Hello {
	
	/**
	 * This is a javadoc comment.
	 * @param args
	 */
	public static void main (String args[])
	{
		System.out.println("Hello World!");
	}

	public String concatenateString (String input)
	{
		return input.concat(" said the dog.");
	}
	
}
