/**
 * 
 */
package com.theosterkamps.hello;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author bryanosterkamp
 *
 */
public class HelloTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.theosterkamps.hello.Hello#concatenateString(java.lang.String)}.
	 */
	@Test
	public void testConcatenateString() {
		Hello hello = new Hello();
		String actual = hello.concatenateString("Hello");
		assertEquals("Checking for hello response", "Hello said the dog.", actual);
		//fail("Not yet implemented"); // TODO
	}

}
